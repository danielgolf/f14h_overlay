# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:$

EAPI=7
inherit cmake

DESCRIPTION="Hantek DSO6022 USB digital signal oscilloscope"
HOMEPAGE="https://github.com/OpenHantek/OpenHantek6022"
SRC_URI="https://github.com/OpenHantek/OpenHantek6022/archive/${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
QTMIN="5.4"

#CMAKE_MAKEFILE_GENERATOR="emake"
#CMAKE_IN_SOURCE_BUILD=1

IUSE=""

DEPEND=">=dev-qt/qtgui-${QTMIN}:5
	>=dev-qt/qtwidgets-${QTMIN}:5
	>=dev-qt/qtopengl-${QTMIN}:5
	dev-libs/libusb:1
	sci-libs/fftw:3.0
	media-libs/mesa[gles2]"

RDEPEND="${DEPEND} sci-electronics/electronics-menu"

KEYWORDS="~amd64"

S="${WORKDIR}/OpenHantek6022-"${PV}

src_prepare() {
	cmake_src_prepare
}

src_configure() {
	cmake_src_configure
}

src_install() {
	cmake_src_install
}

#pkg_postinst() {
#	echo
#	elog "You will need the firmware for your DSO installed."
#	elog "Visit http://www.openhantek.org/ for futher configuration instructions."
#	echo
#}
