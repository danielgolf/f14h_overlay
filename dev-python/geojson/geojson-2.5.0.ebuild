# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_{5..9} )

inherit distutils-r1

DESCRIPTION="Python bindings and utilities for GeoJSON"
HOMEPAGE="https://github.com/geomet/geomet"
SRC_URI="https://files.pythonhosted.org/packages/b6/8d/c42d3fe6f9b5e5bd6a55d9f03813d674d65d853cb12e6bc56f154a2ceca0/${P}.tar.gz"
S="${WORKDIR}/geojson-${PV}"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND=""
