EAPI=7
GCONF_DEBUG=no

DESCRIPTION="C++ bindings for gtkspell"
HOMEPAGE=http://gtkspell.sourceforge.net/
SRC_URI="mirror://sourceforge/project/gtkspell/$PN/$P.tar.xz"

LICENSE=GPL-2+
SLOT=0
KEYWORDS=amd64

RDEPEND=
DEPEND="
	>=app-text/gtkspell-3.0.5
       "

IUSE=doc

src_configure()
{
	ECONF_SOURCE="${S}" default_src_configure \
		--disable-tests \
		$(use_enable doc documentation)
}

