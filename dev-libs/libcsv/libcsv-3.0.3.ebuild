# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit autotools

DESCRIPTION="A simple, fast library to read and write CSV data"
HOMEPAGE="http://sourceforge.net/projects/libcsv/"
SRC_URI="mirror://sourceforge/libcsv/${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="doc examples static-libs"

src_install() {
	autotools-utils_src_install
	if use doc ; then
          dodoc README ChangeLog csv.pdf || die
	else
          dodoc README ChangeLog || die
	fi
	if use examples ; then
	  insinto /usr/share/doc/${PF}/examples
	  doins "${S}/examples/"*.c
	fi
}
